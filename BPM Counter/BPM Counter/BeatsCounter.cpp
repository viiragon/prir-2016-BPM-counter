#include <algorithm>
#include <functional>
#include <string>
#include <thread>
#include "FFT.h"
#include "MP3Decoder.h"
#include "BeatsCounter.h"

#define BANDS_COUNT 8

std::vector<EnergyForBPM> partialResults;

BPM countBPM(const std::string &filename, int samplesSetsCount, bool multiThreading)
{
	BPM bestBPM;
	Complex energyForBestBPM;

	//Check file validity
	register int i = filename.find_last_of('.'), j, k;
	MusicDecoder *decoder = nullptr;
	if(i < 0) throw std::string("Incorrect file format for file " + filename + "!");

	std::string extension = filename.substr(i + 1);
	std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
	if(extension == "mp3")
		decoder = new MP3Decoder(filename.c_str());
	//else if
	else throw std::string("File " + filename + " is of none of supported formats!");

	//Get essential information from file
	SamplesPackage *samplesSets = decoder->getSamples(samplesSetsCount);
	int bitDepth = decoder->sampleSize * 8;
	SamplingRate samplingRate = decoder->sampleRate;
	samplesSetsCount = samplesSets->size(); //We need to know how many samples packs are there ACTUALLY

	//Prepare for multithreading
	std::vector<std::thread> threads;
	partialResults.resize(samplesSetsCount);
	threads.reserve(samplesSetsCount);

	//Prepare samples
	int maxFreq = decoder->packageSize, currentFreq;
	int bandsLimits[BANDS_COUNT + 1];
	std::vector<std::vector<CArray>> processedSamplesSets;

	processedSamplesSets.resize(samplesSets->size());
	bandsLimits[0] = 0;
	
	for(i = BANDS_COUNT, currentFreq = maxFreq; i; --i, currentFreq >>= 1)
		bandsLimits[i] = currentFreq;

	for(i = 0; i < samplesSetsCount; ++i)
	{
		CArray &samples = (*samplesSets)[i];
		int cumulatedSize = 0;
		fft(samples);

		//Prepare subbands out of samples sets
		processedSamplesSets[i].resize(BANDS_COUNT);
		for(j = 0; j < BANDS_COUNT; ++j)
		{
			int bandSize = (bandsLimits[j + 1] - bandsLimits[j]) * samples.size() / (2 * maxFreq);
			processedSamplesSets[i][j].resize(bandSize);
			for(k = 0; k < bandSize; ++k)
			{
				float window = (1. - cos(2. * PI * (double)k / (double)( bandSize - 1) ));
				processedSamplesSets[i][j][k] = samples[j * cumulatedSize + k] * window; //Copy samples, and use Hanning window on them
			}
			cumulatedSize += bandSize;

			//Differentiate samples
			Complex lastSample = 0;
			ifft(processedSamplesSets[i][j]);
			for(k = 0; k < bandSize; ++k)
			{
				if(processedSamplesSets[i][j][k].real() < 0.)
					processedSamplesSets[i][j][k].real(-processedSamplesSets[i][j][k].real());
				if(processedSamplesSets[i][j][k].imag() < 0.)
					processedSamplesSets[i][j][k].imag(-processedSamplesSets[i][j][k].imag());
				Complex tmp = processedSamplesSets[i][j][k] - lastSample;
				lastSample = processedSamplesSets[i][j][k];
				if(tmp.real() > 0. || tmp.imag() > 0.)
					processedSamplesSets[i][j][k] = tmp;
			}

			//Perform FFT on finished band
			fft(processedSamplesSets[i][j]);
		}
	}

	//Count BPM on multiple threads (as many as samples packs)
	for (i = 0; i < samplesSetsCount; ++i)
	{
		std::vector<CArray> &samples = processedSamplesSets[i];
		threads.push_back(std::thread(countBeatsForSamples, i, LOWER_BPM_BOUND, HIGHER_BPM_BOUND, 10., samplingRate, bitDepth, std::cref(samples), decoder->packageSize));
		if(!multiThreading)
		{
			threads[0].join();
			threads.clear();
		}
	}
	for(std::thread &th : threads)
		th.join();

	//Analize initial results
	register int bestResultIndex = 0;
	BPM averageBPM = 0;
	Complex highestEnergy = Complex(0, 0);
	for(EnergyForBPM &res : partialResults)
		averageBPM += res.second;
	averageBPM /= samplesSetsCount;

	for(i = 0; i < samplesSetsCount; ++i)
	{
		if((100. * partialResults[i].second / averageBPM) < 0.9)
			continue; //Counted BPM is at least 10% off the average BPM value - we'll skip it as invalid
		if(std::abs(partialResults[i].first) > std::abs(highestEnergy))
		{
			highestEnergy = partialResults[i].first;
			bestResultIndex = i;
		}
	}
	bestBPM = partialResults[bestResultIndex].second;
	energyForBestBPM = highestEnergy;

	//Further BPM search around first result (with step 0.25)
	BPM currentBPM = bestBPM - 5. - 0.25;
	std::vector<CArray> bestSamples = processedSamplesSets[bestResultIndex];

	//First clear conteners
	partialResults.clear();
	threads.clear();
	partialResults.resize(4); //Arbitrary, it would be easier to divide data on four threads now

	for(i = 0; i < 2; ++i)
	{
		threads.push_back(std::thread( countBeatsForSamples, i, currentBPM, currentBPM + 2.5, 0.25, samplingRate, bitDepth, std::cref(bestSamples), decoder->packageSize ));
		currentBPM += 2.75;
		if (!multiThreading)
		{
			threads[0].join();
			threads.clear();
		}
	}
	for(currentBPM += 0.25; i < (4); ++i) //Second for to avoid counting BPM with highest (so far)
	{
		threads.push_back(std::thread( countBeatsForSamples, i, currentBPM, currentBPM + 2.5, 0.25, samplingRate, bitDepth, std::cref(bestSamples), decoder->packageSize ));
		currentBPM += 2.75;
		if (!multiThreading)
		{
			threads[0].join();
			threads.clear();
		}
	}
	for(std::thread &th : threads)
		th.join();

	for(i = 0, bestResultIndex = -1; i < partialResults.size(); ++i)
	{
		if(std::abs(partialResults[i].first) > std::abs(highestEnergy))
		{
			highestEnergy = partialResults[i].first;
			bestResultIndex = i;
		}
	}
	if(bestResultIndex >= 0)
	{
		bestBPM = partialResults[bestResultIndex].second;
		energyForBestBPM = highestEnergy;
	}

	//Check if 2 * bestBPM is a better result or not
	countBeatsForSamples(0, 2. * bestBPM, 2. * bestBPM, 1., samplingRate, bitDepth, std::cref(bestSamples), decoder->packageSize);
	if(std::abs(partialResults[0].first) > std::abs(energyForBestBPM))
		bestBPM = partialResults[0].second;

	if(decoder) delete decoder;
	return bestBPM;
}

void countBeatsForSamples(int threadNr, BPM lowerRange, BPM upperRange, BPM step, SamplingRate rate, int bitDepth, const std::vector<CArray> &bands, int inputLength)
{
	register int i;
	Sample maxVal = (Sample)((1 << (bitDepth - 1)) - 1);
	Complex peak = Complex(maxVal, maxVal);
	Complex highestEnergy = 0;
	BPM beatsForHighestEnergy = 0;

	for(double currentBPM = lowerRange; currentBPM <= upperRange; currentBPM += step)
	{
		register int ti = (int)(60.F * (BPM)rate / currentBPM);
		Complex energy = 0;

		CArray comb(inputLength);
		for(i = 0; i < inputLength; ++i)
			comb[i] = ((i % ti) ? Complex(0, 0) : peak);
		fft(comb);

		int combIndex = 0;
		for(const CArray &band : bands)
		{
			Complex bandEnergy = 0;
			for(i = 0; i < band.size(); ++i, ++combIndex)
				bandEnergy += band[i] * comb[combIndex];
			if(std::abs(bandEnergy) > std::abs(energy))
				energy = bandEnergy;
		}
		if(std::abs(energy) > std::abs(highestEnergy))
		{
			highestEnergy = energy;
			beatsForHighestEnergy = currentBPM;
		}
	}

	partialResults[threadNr] = {highestEnergy, beatsForHighestEnergy};
}