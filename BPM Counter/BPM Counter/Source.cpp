#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <math.h>


extern "C" {
#include "libavutil/mathematics.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
}

int test() {
	const char* input_filename = "folder\wave.wav";

	//avcodec_register_all();
	av_register_all();
	//av_ini

	AVFormatContext* container = avformat_alloc_context();
	if (avformat_open_input(&container, input_filename, NULL, NULL) < 0){
		std::cout << "Could not open file";
		return 1;
	}

	if (avformat_find_stream_info(container, NULL) < 0){
		std::cout << "Could not find file info";
		return 1;
	}
	av_dump_format(container, 0, input_filename, false);

	int stream_id = -1;
	int i;
	for (i = 0; i < container->nb_streams; i++){
		if (container->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO){
			stream_id = i;
			break;
		}
	}
	if (stream_id == -1){
		std::cout << "Could not find Audio Stream";
		return 1;
	}

	AVDictionary *metadata = container->metadata;

	AVCodecContext *ctx = container->streams[stream_id]->codec;
	AVCodec *codec = avcodec_find_decoder(ctx->codec_id);

	if (codec == NULL){
		std::cout << "cannot find codec!";
		return 1;
	}

	if (avcodec_open2(ctx, codec, NULL) < 0){
		std::cout << "Codec cannot be found";
		return 1;
	}

	AVPacket packet;
	av_init_packet(&packet);

	AVFrame *frame = av_frame_alloc();
		
	int buffer_size = 192000 + FF_INPUT_BUFFER_PADDING_SIZE;;
	uint8_t buffer(buffer_size);
	*packet.data = buffer;
	packet.size = buffer_size;
	
	int len;
	int frameFinished = 0;
	while (av_read_frame(container, &packet) >= 0) {
		if (packet.stream_index == stream_id) {
			//printf("Audio Frame read  \n");
			int len = avcodec_decode_audio4(ctx, frame, &frameFinished, &packet);
			//frame->
			if (frameFinished){
				//printf("Finished reading Frame len : %d , nb_samples:%d buffer_size:%d line size: %d \n",len,frame->nb_samples,buffer_size,frame->linesize[0]);
				printf("%s %d\n", (char*)frame->extended_data[0], frame->linesize[0]);
			}
			else{
				//printf("Not Finished\n");
			}
		} else {
			printf("Someother packet possibly Video\n");
		}
	}
	return 0;
}