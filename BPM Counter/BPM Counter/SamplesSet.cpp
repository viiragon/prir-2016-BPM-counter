#include "SamplesSet.h"

SamplesSet::SamplesSet(float *source, int count, double secs) {
	samples = source;
	samplesCount = count;
	lengthInSeconds = secs;
}

SamplesSet::~SamplesSet() {
	delete samples;
}