#include "MusicDecoder.h"

class MP3Decoder : public MusicDecoder {
public:
	MP3Decoder(const char* fileName) : MusicDecoder(fileName) { samples = nullptr; }
	virtual SamplesPackage *getSamples(int packages) override;
	SamplesPackage *samples;
};

