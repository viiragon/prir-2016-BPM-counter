#include <complex>
#include <utility>
#include <valarray>
#include <vector>

#define PI 3.141592653589793238462643383279502884L

typedef float BPM;
typedef int SamplingRate;
typedef float Sample;
typedef std::complex<Sample> Complex;
typedef std::valarray<Complex> CArray;
typedef std::pair<Complex, BPM> EnergyForBPM;