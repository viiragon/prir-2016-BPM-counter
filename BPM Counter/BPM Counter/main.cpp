﻿//#include co tylko będziemy potrzebować
//nie używamy using namespace std - nie wypada ;_;
//w dobrym tonie jest komentować po angielsku (właśnie widzę ;D), bo tak mnie uczyli w robocie i tak też robię odruchow
//nazwy zmiennych, struktur, klas i procedur ofc też po angielsku
#include <windows.h>
#include <iostream>
#include <string.h>
#include "BeatsCounter.h"
#include "MP3Decoder.h"

#define PACKAGES 20

bool threads = true;
std::vector<std::string> files;

int readFolder(char* folder) {
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(folder, &FindFileData);
	if (hFind != INVALID_HANDLE_VALUE) {
		int multiple = 0;
		int i = 0;
		while (folder[i] != '\0') {
			if (folder[i] == '*' || folder[i] == '?') {
				folder[i] = '\0';
				multiple = 1;
				break;
			}
			i++;
		}
		do {
			char name[1000];
			for (int i = 0; i < 1000; i++) {
				name[i] = 0;
			}
			strcat_s(name, folder);
			if (multiple){
				strcat_s(name, FindFileData.cFileName);
			}
			std::cout << "Parsing file " << name << "...\n";
			try { std::cout << "BPM is (propably) " << countBPM(name, PACKAGES, threads) << std::endl; }
			catch(std::string &err) { std::cout << "Error during BPM counting:\n" << err << std::endl; }
			catch(...) { std::cout << "\nFatal error!\n"; }
		} while (FindNextFile(hFind, &FindFileData));
		FindClose(hFind);
	}
	else {
		std::cerr << "No file associated with name '" << folder << "' found!\n";
	}
	return 0;
}

void showHelp() {
	std::cout << "USAGE:\n./<program> (-t threads) (-h) <files>\n"
		<< "-t threads - disables paralell computing.\n"
		<< "-h - this help\n"
		<< "files - paths to audio files. They can contain wildcard symbols.\n";
}

int main(int argc, char **argv) {
	if (argc > 1) {
		for (int i = 1; i < argc; i++) {
			if (argv[i][0] == '-') {	//Arguments?
				switch (argv[i][1]) {
				case 't':	//Threads
					threads = false;
					std::cout << "No multithreading used!\n";
					break;
				case 'h':
					showHelp();
					std::cin.get();
					exit(0);
					break;
				default:
					std::cerr << "Unknown parameter '" << argv[i] << "'!\n";
					showHelp();
					std::cin.get();
					exit(0);
					return 1;
				}
			}
			else {
				readFolder(argv[i]);
			}
		}
	}
	if (files.capacity() > 0) {
		std::cout << "Files:\n";
		for (size_t i = 0; i < files.size(); i++) {
			std::cout << files[i] << '\n';
		}
	}
	std::cin.get();
	return 0;
}