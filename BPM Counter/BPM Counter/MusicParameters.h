#include "head.h"

class MusicParameters
{
	friend class MusicParametersComnprarator;
	public:
		MusicParameters(BPM bpm, SamplingRate rate, int bits) : beatsPerMinute(bpm), samplingRate(rate), bitDepth(bits)
		{
		}

		BPM getBPM() const
		{
			return beatsPerMinute;
		}

		SamplingRate getSamplingRate() const
		{
			return samplingRate;
		}

		int getBitDepth() const
		{
			return bitDepth;
		}

	private:
		BPM beatsPerMinute;
		SamplingRate samplingRate;
		int bitDepth;
};

class MusicParametersComnprarator
{
	public: bool operator()(const MusicParameters &m1, const MusicParameters &m2) const
	{
		if(m1.beatsPerMinute < m2.beatsPerMinute) return true;
		if(m1.beatsPerMinute > m2.beatsPerMinute) return false;

		if(m1.samplingRate < m2.samplingRate) return true;
		if(m1.samplingRate > m2.samplingRate) return false;

		if(m1.bitDepth < m2.bitDepth) return true;
		/*if(m1.bitDepth > m2.bitDepth)*/ return false;
	}
};