#define AVCODEC_MAX_AUDIO_FRAME_SIZE 192000 
#define INBUF_SIZE 4096
#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096

#include "MP3Decoder.h"
#include <iostream>
#include <math.h>

#include <io.h>
#include <stdio.h>
extern "C"
{
#include "libavutil/opt.h"
#include "libavcodec/avcodec.h"
#include "libavutil/channel_layout.h"
#include "libavutil/common.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libavformat/avformat.h"
}
#pragma comment(lib, "lib/avcodec.lib")
#pragma comment(lib, "lib/avutil.lib")
#pragma comment(lib, "lib/avformat.lib")

#define SAMPLING 44100
#define TAG_ANAYSIS 0	//If you want to analyse tags, change this to 1. But tags dont contain any useful audio info :(

void tagAnalysis(FILE *f, uint8_t inbuf[]);
void finalize(FILE *file, AVCodecContext *c, AVFrame *decoded_frame);
Complex sumBuffors(uint8_t **buff, int channels, int index, int sampleSize);
int getPackageSize(int sampleRate);

SamplesPackage *MP3Decoder::getSamples(int packageNumber) {
	SamplesPackage *data = NULL;

	AVCodec *codec;
	AVCodecContext *c = NULL;
	int len;
	FILE *f = NULL;
	uint8_t inbuf[AUDIO_INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];
	AVPacket avpkt;
	AVFrame *decoded_frame = NULL;

	av_init_packet(&avpkt);
	avcodec_register_all();	//This is neccesary for some reason...
	av_log_set_level(AV_LOG_QUIET);

	/* find the mpeg audio decoder */
	codec = avcodec_find_decoder(AV_CODEC_ID_MP3);
	if (!codec) {
		fprintf(stderr, "Codec not found\n");
		finalize(f, c, decoded_frame);
		return NULL;
	}

	c = avcodec_alloc_context3(codec);
	if (!c) {
		fprintf(stderr, "Could not allocate audio codec context\n");
		finalize(f, c, decoded_frame);
		return NULL;
	}

	/* open it */
	if (avcodec_open2(c, codec, NULL) < 0) {
		fprintf(stderr, "Could not open codec\n");
		finalize(f, c, decoded_frame);
		return NULL;
	}

	fopen_s(&f, fileName, "rb");
	if (!f) {
		fprintf(stderr, "Could not open %s\n", fileName);
		finalize(f, c, decoded_frame);
		return NULL;
	}

	tagAnalysis(f, inbuf);

	avpkt.data = inbuf;
	avpkt.size = (int)fread(inbuf, 1, AUDIO_INBUF_SIZE, f);

	int init = 1;
	int current = 0;
	int package = 0;
	int errorNum = 0;

	while (avpkt.size > 0) {
		int got_frame = 0;

		if (!decoded_frame) {
			if (!(decoded_frame = av_frame_alloc())) {
				fprintf(stderr, "Could not allocate audio frame\n");
				finalize(f, c, decoded_frame);
				return NULL;
			}
		}

		if ((int)*(avpkt.data) == 255) {
			len = avcodec_decode_audio4(c, decoded_frame, &got_frame, &avpkt);
		} else {
			len = 0;
		}

		if (len <= 0) {
			avpkt.size -= 1;
			avpkt.data += 1;
			avpkt.dts = avpkt.pts = AV_NOPTS_VALUE;
			if (avpkt.size < AUDIO_REFILL_THRESH) {	//Refilling input buffer
				memmove(inbuf, avpkt.data, avpkt.size);
				avpkt.data = inbuf;
				len = (int)fread(avpkt.data + avpkt.size, 1, AUDIO_INBUF_SIZE - avpkt.size, f);
				if (len > 0)
					avpkt.size += len;
			}
			errorNum++;
			if (avpkt.size > 0)
				continue;
			else
				break;
		}
		if (init) {
			init = 0;
			channels = decoded_frame->channels;
			sampleRate = decoded_frame->sample_rate;
			sampleSize = av_get_bytes_per_sample(c->sample_fmt);
			packageSize = getPackageSize(sampleRate);
			data = new SamplesPackage(packageNumber);
			for(int i = 0; i < data->size(); ++i)
				(*data)[i] = CArray(packageSize);
		}

		if (got_frame) { //if a frame has been decoded, output it
			if (sampleSize < 0) {
				fprintf(stderr, "Failed to calculate data size\n");
				finalize(f, c, decoded_frame);
				return NULL;
			}

			data[0].reserve(packageSize);
			for (int i = 0; i < decoded_frame->linesize[0]; i += sampleSize) {
				if (package < packageNumber) {	//dont get too much into data
					(*data)[package][current] = sumBuffors(decoded_frame->data, decoded_frame->channels, i, sampleSize);
					current++;
					if (current >= packageSize) {
						current = 0;
						package++;
					}
				}
				else {
					break;
				}
			}
		}
		avpkt.size -= len;
		avpkt.data += len;
		avpkt.dts = avpkt.pts = AV_NOPTS_VALUE;
		if (avpkt.size < AUDIO_REFILL_THRESH) {	//Refilling input buffer
			memmove(inbuf, avpkt.data, avpkt.size);
			avpkt.data = inbuf;
			len = (int)fread(avpkt.data + avpkt.size, 1, AUDIO_INBUF_SIZE - avpkt.size, f);
			if (len > 0)
				avpkt.size += len;
		}
	}
	if (errorNum) {
		//std::cout << "Error occured " << errorNum << " times\n";
	}

	if (current == 0) {
		packageNumber = package;
	}
	else {
		packageNumber = package - 1;
	}

	finalize(f, c, decoded_frame);
	return data;
}

void finalize(FILE *file, AVCodecContext *c, AVFrame *decoded_frame) {
	if (file) {
		fclose(file);
	}
	if (c) {
		avcodec_close(c);
		av_free(c);
	}
	if (decoded_frame) {
		av_frame_free(&decoded_frame);
	}
}

Complex sumBuffors(uint8_t **buff, int channels, int index, int sampleSize)
{
	Sample channelsVals[2];
	for(int c = 0; c < channels && c < 2; ++c)
	{
		uint64_t val = 0;
		for(int s = 0; s < sampleSize; ++s)
			val = (val << 8) | buff[c][index + s];
		channelsVals[c] = val;
	}
	return Complex(channelsVals[0], channelsVals[1]);
}

int getPackageSize(int sampleRate) {
	int size = 2;
	sampleRate *= 2;
	while (size < sampleRate) {
		size <<= 2;
	}
	return size;
}

void tagAnalysis(FILE *f, uint8_t inbuf[]) {
	int len = (int)fread(inbuf, 1, 3, f);

	if (inbuf[0] == 'I' && inbuf[1] == 'D' && inbuf[2] == '3') {	//Check if ID3 tag is in
		len = (int)fread(inbuf, 1, 7, f);
		int version = (int)inbuf[0];
		int size = inbuf[3] * 2097152 + inbuf[4] * 16384 + inbuf[5] * 128 + inbuf[6];
		if (size) {std::cout << "\n\nID3 TAG SIZE: " << size << std::endl;
			len = (int)fread(inbuf, 1, size, f);
			if (TAG_ANAYSIS) {
				//Unfortunately tags dont contain bitrate, length, channels ... etc. 
				//so I pretty much wasted my time here....
				//But this is a method to read them if you want <(^.^<)
				int i = 0;
				int tagSize;
				char tag[] = "TEMP";
				int nullTag = 0;
				while (i + 10 < size) {
					for (int j = 0; j < 4; j++) {
						tag[j] = inbuf[i + j];
					}
					tagSize = inbuf[i + 4] * 16777216 + inbuf[i + 5] * 65536 + inbuf[i + 6] * 256 + inbuf[i + 7];
					if (tag[0] != 0) {
						std::cout << "Tag found : " << tag << "\n\tsize : " << tagSize << " B\n";
						if (strcmp(tag, "TIT2") == 0
							|| strcmp(tag, "TPE1") == 0
							|| strcmp(tag, "TYER") == 0
							|| strcmp(tag, "TALB") == 0
							|| strcmp(tag, "TRCK") == 0
							|| strcmp(tag, "TCON") == 0
							|| strcmp(tag, "TDRC") == 0) {
							std::cout << "\tContent : ";
							for (int j = 0; j < tagSize; j++) {
								std::cout << inbuf[i + 10 + j];
							}
							std::cout << "\n\n";
						}
					}
					else {
						nullTag++;
					}
					i += 10 + tagSize;
				}
				std::cout << nullTag << " NULL tags...\n";
			}
		}
	}
	else {
		rewind(f);
	}
}