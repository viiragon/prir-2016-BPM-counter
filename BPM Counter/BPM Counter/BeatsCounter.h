#pragma once

#include <map>
#include "MusicParameters.h"
#include "head.h"

#define LOWER_BPM_BOUND 60
#define HIGHER_BPM_BOUND 120

typedef std::map<MusicParameters, CArray, MusicParametersComnprarator>::iterator TransformatesMapIterator;

BPM countBPM(const std::string &filename, int samplesSetsCount, bool multiThreading);
void countBeatsForSamples(int threadNr, BPM lowerRange, BPM upperRange, BPM step, SamplingRate rate, int bitDepth, const std::vector<CArray> &bands, int inputLength);