#include "head.h"

typedef std::vector<CArray> SamplesPackage;

class MusicDecoder {
	//This must be a class, all right, since there will be more classes realising specific interface definied here
public:
	MusicDecoder(const char* fileName); //May or may not have arguments - to do (for sure - need a filename, but what else?)
	virtual SamplesPackage *getSamples(int packages) = 0; //Pure virtual method to override by other dependant classes
	
	int sampleRate;
	int channels;
	int sampleSize; //in bytes
	int packageSize; //in samples
	int packageNumber;
	SamplesPackage *samples;

protected:
	const char* fileName;
	//Whatever else
};